# LearnReact

To install the base project, run the following command:
    
    npm create vite@latest learnreact

## Creating a File Structure:

    public/
    |_assets/
    | |_css/
    | | |_materialize.css
    | | |_...
    | |_images/
    | | |_favicon.ico
    | | |_...
    | |_svg/
    | | |_icon.svg
    | | |_...
    | |_...
    |_index.jsx
    |_robots.txt
    |_.htaccess
    |_...
    src/
    |_assets/
    | |_css/
    | | |_App.css
    | | |_Calendary.css
    | | |_...
    | |_images/
    | | |_favicon.ico
    | | |_...
    | |_svg/
    | | |_icon.svg
    | | |_...
    | |_index.css
    | |_...
    |_components/
    | |_Context/
    | | |_index.jsx
    | | |_...
    | |_Instructions/
    | | |_index.jsx
    | | |_emoji.jsx
    | | |_form.jsx
    | | |_button.jsx
    | | |_...
    | |_Calendary/
    | | |_button.jsx
    | | |_...
    | |_...
    |_conf/
    | |_app.jsx
    | |_...
    |_data/
    | |_app.json
    | |_...
    |_error/
    | |_404.jsx
    | |_...
    |_lang/
    | |_es/
    | | |_auth.jsx
    | | |_password.jsx
    | | |_...
    | |_en/
    | | |_validation.jsx
    | | |_...
    |_pdf/
    | |_index.jsx
    | |_form.jsx
    | |_...
    |_router/
    | |_index.jsx
    |_store/
    | |_index.jsx
    |_tests/
    | |_components/
    | | |_index.spec.jsx
    | | |_...
    | |_%test%.jsx - Example.spec.jsx or Example.test.jsx
    | |_...
    |_views/
    | |_Dashboard/
    | | |_index.jsx
    | | |_...
    | |_About/
    | | |_index.jsx
    | | |_...
    | |_Log/
    | | |_index.jsx
    | | |_...
    | |_app.jsx
    | |_...
    |_index.jsx
    |_...

### Observation:

the compilator vite - is funtional at file with the extension .jsx. the error syntax with .js is the file.