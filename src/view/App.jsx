import { useState } from 'react'
import reactLogo from '../assets/svg/react.svg'
import reduxLogo from '../assets/svg/redux.svg'
import '../assets/css/App.css';

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="./assets/svg/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
        <a href="https://es.reactjs.org/" target="_blank" className='es'>
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
        <a href="https://redux-toolkit.js.org/" target="_blank">
          <img src={reduxLogo} className="logo redux" alt="Redux logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
